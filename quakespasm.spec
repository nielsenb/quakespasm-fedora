Name:		quakespasm
Version:	0.96.0
Release:	1%{?dist}
Summary:	The Quakespasm engine for iD software's Quake
Group:		Games
License:	GPL-2.0-or-later
URL:		http://quakespasm.sourceforge.net
Source0:	https://sourceforge.net/projects/quakespasm/files/Source/quakespasm-%{version}.tar.gz
Source1:	%{name}.desktop
BuildRequires:	desktop-file-utils
BuildRequires:	make
BuildRequires:	gcc
BuildRequires:	pkgconfig(gl)
BuildRequires:  pkgconfig(mad)
BuildRequires:	pkgconfig(sdl2)
BuildRequires:	pkgconfig(vorbis)

%description
An engine for iD software's Quake.

Worlds; tortured, scale massive and with pestilence.

The road unbroken. This vehicle a bastille of C and OpenGL. Weatherd and beaten
by Carmack, Fitzgibbons, and journeymen. Coaxed with SDL for Unix.

%prep
%setup -q -c -n quakespasm

%build
cd %{name}-%{version}/Quake
CFLAGS="%{optflags}" %make_build DEBUG=1 DO_USERDIRS=1 USE_SDL2=1

%install
#Install the icon
%{__install} -D -p -m 644 %{name}-%{version}/Misc/QuakeSpasm_512.png \
	%{buildroot}%{_datadir}/icons/hicolor/512x512/apps/%{name}.png

mkdir -p %{buildroot}%{_bindir}
cp %{name}-%{version}/Quake/%{name} %{buildroot}%{_bindir}

#Install the desktop file
desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{SOURCE1}

%files
%license %{name}-%{version}/LICENSE.txt
%{_bindir}/%{name}
%{_datarootdir}/icons/hicolor/512x512/apps/%{name}.png
%{_datarootdir}/applications/%{name}.desktop

%changelog
* Fri Sep 15 2023 Brandon Nielsen <nielsenb@jetfuse.net> 0.96.0-1
- Update to 0.96.0
- Adopt SPDX for the license field

* Mon Jan 30 2023 Brandon Nielsen <nielsenb@jetfuse.net> 0.95.1-1
- Update to 0.95.1

* Sun Aug 21 2022 Brandon Nielsen <nielsenb@jetfuse.net> 0.94.7-1
- Update to 0.94.7

* Sat Feb 26 2022 Brandon Nielsen <nielsenb@jetfuse.net> 0.94.3-1
- Update to 0.94.3

* Sun Nov 14 2021 Brandon Nielsen <nielsenb@jetfuse.net> 0.94.2-1
- Update to 0.94.2
- Correct URL
- Use https for source download
- Simplify install step
- Use optflags for build
- Pass DEBUG=1 to build to make generating debuginfo work

* Thu Sep 9 2021 Brandon Nielsen <nielsenb@jetfuse.net> 0.94.1-1
- Update to 0.94.1
- Enable mp3 soundtrack support

* Fri May 22 2020 Brandon Nielsen <nielsenb@jetfuse.net> 0.93.2-2
- Use pkgconfig for BuildRequires
- Explicitly require make

* Fri Jan 17 2020 Brandon Nielsen <nielsenb@jetfuse.net> 0.93.2-1
- Update to 0.93.2
- Add dist tag to release

* Wed Oct 10 2018 Brandon Nielsen <nielsenb@jetfuse.net> 0.93.1-1
- Update to 0.93.1

* Thu Mar 29 2018 Brandon Nielsen <nielsenb@jetfuse.net> 0.93.0-2
- Specfile cleanup
- Package license file

* Fri Dec 22 2017 Brandon Nielsen <nielsenb@jetfuse.net> 0.93.0-1
- Upgrade to 0.93.0

* Thu Mar 30 2017 Brandon Nielsen <nielsenb@jetfuse.net> 0.92.1-2
- Add desktop file

* Fri Sep 23 2016 Brandon Nielsen <nielsenb@jetfuse.net> 0.92.1-1
- Upgrade to 0.92.1

* Fri Jun 24 2016 Brandon Nielsen <nielsenb@jetfuse.net> 0.91.0-1
- Upgrade to 0.91.0
- Switch to SDL2

* Thu Oct 15 2015 Brandon Nielsen <nielsenb@jetfuse.net> 0.85.9-3
- Disable building debuginfo package to fix issues building on F23.

* Thu Jul 10 2014 Brandon Nielsen <nielsenb@jetfuse.net> 0.85.9-1
- Initial specfile
