===================
 quakespasm-fedora
===================

Nothing but a specfile for building an RPM with both mp3 and ogg support.

See the official `Quakespasm`_ page for more details.

.. _Quakespasm: http://quakespasm.sourceforge.net/about.htm
